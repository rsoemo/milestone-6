package com.example.milestone6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity
{
    //======Class=Variables======//
    //===Contact=GUI=Elements===//
    ImageView iv_main_ContactPhoto1, iv_main_ContactPhoto2, iv_main_ContactPhoto3, iv_main_ContactPhoto4;
    TextView tv_main_ContactName1, tv_main_ContactName2, tv_main_ContactName3, tv_main_ContactName4;
    ImageButton imgbtn_main_ContactEdit1, imgbtn_main_ContactEdit2, imgbtn_main_ContactEdit3, imgbtn_main_ContactEdit4;
    ImageButton imgbtn_main_ContactDelete1, imgbtn_main_ContactDelete2, imgbtn_main_ContactDelete3, imgbtn_main_ContactDelete4;
    EditText et_focus_ContactName, et_focus_ContactPhoneNumber, et_focus_ContactLocation;
    //===Other=Buttons=GUI=Elements===//
    TextView tv_main_Title, tv_main_Page, tv_focus_Title;
    Button btn_main_Previous, btn_main_Next, btn_focus_Submit, btn_main_Create;
    ImageButton imgbtn_focus_Cancel;
    ImageView iv_focus_ContactImage;
    //===Other=Variables===//
    public ArrayList<Contact> contactList = new ArrayList<Contact>();
    public ArrayList<Contact> currentPageList = new ArrayList<Contact>();
    public Contact blank = new Contact("", 0, "");
    public Contact editingContact = blank;
    int currentPage = 1;
    int pageCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //===Instantiation===//
        instantiateData();  //Creating Data
        super.onCreate(savedInstanceState);
        loadMainPage();  //Loading main page
    }

    //========================//
    //======loadMainPage======//
    //========================//
    public void loadMainPage()
    {
        //======Loading=Page======//
        setContentView(R.layout.activity_main);

        //======GUI=Element=Defining======//
        //===Contact=Photo===//
        iv_main_ContactPhoto1 = findViewById(R.id.iv_main_ContactPhoto1);
        iv_main_ContactPhoto2 = findViewById(R.id.iv_main_ContactPhoto2);
        iv_main_ContactPhoto3 = findViewById(R.id.iv_main_ContactPhoto3);
        iv_main_ContactPhoto4 = findViewById(R.id.iv_main_ContactPhoto4);
        //===Contact=Name===//
        tv_main_ContactName1 = findViewById(R.id.tv_main_ContactName1);
        tv_main_ContactName2 = findViewById(R.id.tv_main_ContactName2);
        tv_main_ContactName3 = findViewById(R.id.tv_main_ContactName3);
        tv_main_ContactName4 = findViewById(R.id.tv_main_ContactName4);
        //===Contact=Edit===//
        imgbtn_main_ContactEdit1 = findViewById(R.id.imgbtn_main_ContactEdit1);
        imgbtn_main_ContactEdit2 = findViewById(R.id.imgbtn_main_ContactEdit2);
        imgbtn_main_ContactEdit3 = findViewById(R.id.imgbtn_main_ContactEdit3);
        imgbtn_main_ContactEdit4 = findViewById(R.id.imgbtn_main_ContactEdit4);
        //===Contact=Delete===//
        imgbtn_main_ContactDelete1 = findViewById(R.id.imgbtn_main_ContactDelete1);
        imgbtn_main_ContactDelete2 = findViewById(R.id.imgbtn_main_ContactDelete2);
        imgbtn_main_ContactDelete3 = findViewById(R.id.imgbtn_main_ContactDelete3);
        imgbtn_main_ContactDelete4 = findViewById(R.id.imgbtn_main_ContactDelete4);
        //===Other=Elements===//
        tv_main_Title = findViewById(R.id.tv_main_Title);
        tv_main_Page = findViewById(R.id.tv_main_Page);
        btn_main_Previous = findViewById(R.id.btn_main_Previous);
        btn_main_Next = findViewById(R.id.btn_main_Next);
        btn_main_Create = findViewById(R.id.btn_main_Create);
        //===Null=Check===//
        if (iv_main_ContactPhoto1 == null || iv_main_ContactPhoto2 == null || iv_main_ContactPhoto3 == null || iv_main_ContactPhoto4 == null ||
                tv_main_ContactName1 == null || tv_main_ContactName2 == null || tv_main_ContactName3 == null || tv_main_ContactName4 == null ||
                imgbtn_main_ContactEdit1 == null || imgbtn_main_ContactEdit2 == null || imgbtn_main_ContactEdit3 == null || imgbtn_main_ContactEdit4 == null ||
                tv_main_Title == null || tv_main_Page == null || btn_main_Previous == null || btn_main_Next == null || btn_main_Create == null)
        {
            Log.d("loadMainPage", "WARNING: One or more elements have been detected as null");
        }

        //======Updating=Page=Contents======//
        updatePage();

        //======Element=Listeners======//
        //===Next=Page===//
        btn_main_Next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (currentPage < pageCount)
                {
                    currentPage++;
                    updatePage();
                }
            }
        });
        //===Previous=Page===//
        btn_main_Previous.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (1 < currentPage)
                {
                    currentPage--;
                    updatePage();
                }
            }
        });
        //===Create=Contact===//
        btn_main_Create.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loadCreateContact();
            }
        });
        //===Edit=Contact=1===//
        imgbtn_main_ContactEdit1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loadEditContact(1);
            }
        });
        //===Edit=Contact=2===//
        imgbtn_main_ContactEdit2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loadEditContact(2);
            }
        });
        //===Edit=Contact=3===//
        imgbtn_main_ContactEdit3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loadEditContact(3);
            }
        });
        //===Edit=Contact=4===//
        imgbtn_main_ContactEdit4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                loadEditContact(4);
            }
        });
        //===Remove=Contact=1===//
        imgbtn_main_ContactDelete1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                contactList.remove(currentPageList.get(0));
                saveData();
                updatePage();
            }
        });
        //===Remove=Contact=2===//
        imgbtn_main_ContactDelete2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                contactList.remove(currentPageList.get(1));
                saveData();
                updatePage();
            }
        });
        //===Remove=Contact=3===//
        imgbtn_main_ContactDelete3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                contactList.remove(currentPageList.get(2));
                saveData();
                updatePage();
            }
        });
        //===Remove=Contact=4===//
        imgbtn_main_ContactDelete4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                contactList.remove(currentPageList.get(3));
                saveData();
                updatePage();
            }
        });
    }

    //=============================//
    //======loadCreateContact======//
    //=============================//
    public void loadCreateContact()
    {
        //======Loading=Page======//
        setContentView(R.layout.activity_focuscontact);

        //======GUI=Element=Defining======//
        //===Contact=Elements===//
        et_focus_ContactName = findViewById(R.id.et_focus_ContactName);
        et_focus_ContactPhoneNumber = findViewById(R.id.et_focus_ContactPhoneNumber);
        et_focus_ContactLocation = findViewById(R.id.et_focus_ContactLocation);
        iv_focus_ContactImage = findViewById(R.id.iv_focus_ContactImage);
        //===Menu=Elements===//
        tv_focus_Title = findViewById(R.id.tv_focus_Title);
        btn_focus_Submit = findViewById(R.id.btn_focus_Submit);
        imgbtn_focus_Cancel = findViewById(R.id.imgbtn_focus_Cancel);
        //===Null=Check===//
        if (et_focus_ContactName == null || et_focus_ContactPhoneNumber == null || et_focus_ContactLocation == null || tv_focus_Title == null ||
                btn_focus_Submit == null || iv_focus_ContactImage == null)
        {
            Log.d("loadEditContact", "WARNING: One or more elements have been detected as null");
        }

        tv_focus_Title.setText(R.string.focus_creating);  //Setting Title

        //===Cancel=Edit===//
        imgbtn_focus_Cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                editingContact = blank;
                loadMainPage();
            }
        });

        //===Save=Edit===//
        btn_focus_Submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String contactName = et_focus_ContactName.getText().toString();
                int contactPhoneNumber = 0;
                try
                {
                    contactPhoneNumber = Integer.parseInt(et_focus_ContactPhoneNumber.getText().toString());
                }
                catch (Exception e){}
                String contactLocation = et_focus_ContactLocation.getText().toString();

                Contact newContact = new Contact(contactName, contactPhoneNumber, contactLocation);

                contactList.add(newContact);
                saveData();
                loadMainPage();
            }
        });
    }


    //===========================//
    //======loadEditContact======//
    //===========================//
    public void loadEditContact(int contactBeingEdited)
    {
        //======Loading=Page======//
        setContentView(R.layout.activity_focuscontact);

        //======GUI=Element=Defining======//
        //===Contact=Elements===//
        et_focus_ContactName = findViewById(R.id.et_focus_ContactName);
        et_focus_ContactPhoneNumber = findViewById(R.id.et_focus_ContactPhoneNumber);
        et_focus_ContactLocation = findViewById(R.id.et_focus_ContactLocation);
        iv_focus_ContactImage = findViewById(R.id.iv_focus_ContactImage);
        //===Menu=Elements===//
        tv_focus_Title = findViewById(R.id.tv_focus_Title);
        btn_focus_Submit = findViewById(R.id.btn_focus_Submit);
        imgbtn_focus_Cancel = findViewById(R.id.imgbtn_focus_Cancel);
        //===Null=Check===//
        if (et_focus_ContactName == null || et_focus_ContactPhoneNumber == null || et_focus_ContactLocation == null || tv_focus_Title == null ||
                btn_focus_Submit == null || iv_focus_ContactImage == null)
        {
            Log.d("loadEditContact", "WARNING: One or more elements have been detected as null");
        }

        //======Printing=Contact======//
        editingContact = currentPageList.get(contactBeingEdited - 1);  //Getting contact to edit
        //===Storing=Contact=Info===//
        String contactName = editingContact.getContactName();
        String contactPhoneNumber = Integer.toString(editingContact.getContactPhoneNumber());
        String contactLocation = editingContact.getContactLocation();
        //===Printing=Contact=Info===//
        tv_focus_Title.setText(R.string.focus_editing);
        et_focus_ContactName.setText(contactName);
        et_focus_ContactPhoneNumber.setText(contactPhoneNumber);
        et_focus_ContactLocation.setText(contactLocation);

        //======Element=Listeners======//
        //===Cancel=Edit===//
        imgbtn_focus_Cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                editingContact = blank;
                loadMainPage();
            }
        });
        //===Save=Edit===//
        btn_focus_Submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                editingContact.setContactName(et_focus_ContactName.getText().toString());
                try
                {
                    editingContact.setContactPhoneNumber(Integer.parseInt(et_focus_ContactPhoneNumber.getText().toString()));
                }
                catch (Exception e){}
                editingContact.setContactLocation(et_focus_ContactLocation.getText().toString());
                contactList.set(contactBeingEdited - 1 + (currentPage - 1), editingContact);
                saveData();
                loadMainPage();
            }
        });
    }

    public void updatePage()
    {
        //===Getting=Updates===//
        currentPageList = pullForMenu(currentPage);  //Pulling Page
        generatePageCount();  //Generating Page Number

        //===Updating=Pages===//
        tv_main_Page.setText(Integer.toString(currentPage) + "/" + Integer.toString(pageCount));  //Updating pages text

        printMenuPage(currentPageList);  //Updating Page
    }

    private void printMenuPage(ArrayList<Contact> pageItems)
    {
        //===Adding=Item=One===//
        tv_main_ContactName1.setText(pageItems.get(0).getContactName());  //Name

        //===Adding=Item=Two===//
        tv_main_ContactName2.setText(pageItems.get(1).getContactName());  //Name

        //===Adding=Item=Three===//
        tv_main_ContactName3.setText(pageItems.get(2).getContactName());  //Name

        //===Adding=Item=Four===//
        tv_main_ContactName4.setText(pageItems.get(3).getContactName());  //Name
    }

    public ArrayList<Contact> pullForMenu(int page) {
        //======Creating=Variables=======//
        int calculatingNumber = page - 1;
        ArrayList<Contact> pageContent = new ArrayList<Contact>();

        //======Getting=Content=For=Page======//
        //===Adding=Item=One===//
        try {
            pageContent.add(contactList.get(0 + (4 * calculatingNumber)));
        } catch (Exception e) {
            pageContent.add(blank);
        }
        //===Adding=Item=Two===//
        try {
            pageContent.add(contactList.get(1 + (4 * calculatingNumber)));
        } catch (Exception e) {
            pageContent.add(blank);
        }
        //===Adding=Item=Three===//
        try {
            pageContent.add(contactList.get(2 + (4 * calculatingNumber)));
        } catch (Exception e) {
            pageContent.add(blank);
        }
        //===Adding=Item=Four===//
        try {
            pageContent.add(contactList.get(3 + (4 * calculatingNumber)));
        } catch (Exception e) {
            pageContent.add(blank);
        }

        //======Returning=List======//
        return pageContent;
    }

    public void generatePageCount()
    {
        if ((contactList.size() % 4) != 0)  //If the total count has a remainder after being divided by 4
        {
            pageCount = (contactList.size() / 4) + 1;
        }
        else
        {
            pageCount = (contactList.size() / 4);
        }
    }

    private void saveData()
    {
        try {
            FileOutputStream fileout=openFileOutput("ContactStorage.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);

            for (int timesLooped = 0; timesLooped < contactList.size(); timesLooped++)
            {
                Contact current = contactList.get(timesLooped);  // Getting data to store
                String split = ",";  // Storing split for ease of use
                outputWriter.write("CONTACT"+split+current.getContactName()+split+current.getContactPhoneNumber()  // Printing to doc
                        +split+current.getContactLocation()+"\n\r");
            }
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void instantiateData()
    {
        Log.d("File IO", "Starting File IO");
        try {
            FileInputStream filein=openFileInput("ContactStorage.txt");
            InputStreamReader inputReader=new InputStreamReader(filein);

            Boolean contactsNotLoaded = true;
            while(contactsNotLoaded == true)
            {
                Scanner cs = new Scanner(inputReader);  // Creating scanner for file
                while (cs.hasNextLine())
                {
                    String line = cs.nextLine();  // Getting next line
                    String[] items = new String[3];  // Creating storage
                    items = line.split(",");  //Scanning for separator

                    String type = items[0];

                    if (type.compareTo("CONTACT") == 0)
                    {
                        String name = items[1];  // Storing name
                        //int phoneNumber = Integer.valueOf(items[2]);  // Storing phoneNumber
                        int phoneNumber = Integer.valueOf(items[2]);  // Storing phoneNumber
                        String location = items[3];  // Storing location

                        Contact c = new Contact(name, phoneNumber, location);  // Creating new photo
                        contactList.add(c);  // adding new contact
                    }
                }
                contactsNotLoaded = false;
            }
            Log.d("File IO", "Completed reading file");
            filein.close();
        } catch (Exception e) {
            Log.d("File IO", "Exception entered");
            e.printStackTrace();
        }
    }
}