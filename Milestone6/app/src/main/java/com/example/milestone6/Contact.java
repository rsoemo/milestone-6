package com.example.milestone6;

public class Contact
{
    private String contactName;
    private int contactPhoneNumber;
    private String contactLocation;

    public Contact(String contactName, int contactPhoneNumber, String contactLocation) {
        this.contactName = contactName;
        this.contactPhoneNumber = contactPhoneNumber;
        this.contactLocation = contactLocation;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public int getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(int contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getContactLocation() {
        return contactLocation;
    }

    public void setContactLocation(String contactLocation) {
        this.contactLocation = contactLocation;
    }
}